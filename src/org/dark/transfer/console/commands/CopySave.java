package org.dark.transfer.console.commands;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.characters.SkillSpecAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.CoreScript;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import com.fs.starfarer.api.loading.FighterWingSpecAPI;
import com.fs.starfarer.api.loading.HullModSpecAPI;
import com.fs.starfarer.api.loading.WeaponGroupSpec;
import com.fs.starfarer.api.loading.WeaponGroupType;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import data.scripts.campaign.events.SWP_IBBTracker;
import data.scripts.campaign.missions.SWP_FamousBountyEvent.FamousBountyStage;
import exerelin.campaign.AllianceManager;
import exerelin.campaign.PlayerFactionStore;
import exerelin.campaign.alliances.Alliance;
import exerelin.utilities.ExerelinConfig;
import exerelin.utilities.ExerelinUtilsFaction;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.Deflater;
import org.apache.commons.codec.binary.Base64;
import org.lazywizard.console.BaseCommand;
import org.lazywizard.console.CommonStrings;
import org.lazywizard.console.Console;

public class CopySave implements BaseCommand {

    private static final String ARG_HELP = "-help";
    private static final String ARG_UNCOMPRESSED = "-uncompressed";

    private static final List<Character> MAGIC_CHARS = new ArrayList<>(5);

    static final int BLOCK_SIZE = 1024;

    static {
        MAGIC_CHARS.add('{');
        MAGIC_CHARS.add('}');
        MAGIC_CHARS.add('|');
        MAGIC_CHARS.add('\\');
        MAGIC_CHARS.add(':');
    }

    private static String compressData(String input) throws Exception {
        byte[] data = input.getBytes("UTF-8");

        List<byte[]> fullOutput = new ArrayList<>(10);
        byte[] result;
        Deflater compresser = new Deflater();
        compresser.setInput(data);
        compresser.finish();
        int compressedDataLength;
        while (true) {
            result = new byte[BLOCK_SIZE];
            compressedDataLength = compresser.deflate(result);
            if (compressedDataLength == BLOCK_SIZE) {
                fullOutput.add(result);
            } else {
                break;
            }
        }
        compresser.end();

        byte[] output = new byte[compressedDataLength + BLOCK_SIZE * fullOutput.size()];
        int index = 0;
        for (byte[] bytes : fullOutput) {
            System.arraycopy(bytes, 0, output, index * BLOCK_SIZE, BLOCK_SIZE);
            index++;
        }
        System.arraycopy(result, 0, output, index * BLOCK_SIZE, compressedDataLength);
        output = Base64.encodeBase64(output);

        return new String(output, "US-ASCII");
    }

    private static Collection<String> getBuiltInHullMods(ShipVariantAPI variant) {
        ShipVariantAPI tmp = variant.clone();
        tmp.clearHullMods();
        return tmp.getHullMods();
    }

    // Naturally, we can't use actual serialization, so it's gotta be done manually
    @SuppressWarnings("unchecked")
    private static String serializePlayerData() {
        boolean nexerelinExists = Global.getSettings().getModManager().isModEnabled("nexerelin");
        CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
        MutableCharacterStatsAPI playerStats = playerFleet.getCommander().getStats();
        CargoAPI playerCargo = playerFleet.getCargo();
        FleetDataAPI playerFleetData = playerFleet.getFleetData();

        CargoAPI cargoCopy = Global.getFactory().createCargo(true);
        for (CargoStackAPI stack : playerCargo.getStacksCopy()) {
            cargoCopy.addFromStack(stack);
        }

        Map<Object, CargoAPI> allStorage = new LinkedHashMap<>(10);
        CargoAPI omnifactory = null;

        List<SectorEntityToken> entities = new ArrayList<>(Global.getSector().getHyperspace().getEntities(
                                SectorEntityToken.class));
        for (StarSystemAPI system : Global.getSector().getStarSystems()) {
            entities.addAll(system.getEntities(SectorEntityToken.class));
        }
        for (SectorEntityToken entity : entities) {
            if (entity == playerFleet) {
                continue;
            }
            MarketAPI market = entity.getMarket();
            if (market != null) {
                for (SubmarketAPI submarket : market.getSubmarketsCopy()) {
                    if (submarket.getFaction().getId().equals(Factions.PLAYER) && submarket.getPlugin().isFreeTransfer()) {
                        allStorage.put(market, submarket.getCargo());
                    }
                    if (submarket.getSpecId().contentEquals("omnifac_market")) {
                        omnifactory = submarket.getCargo();
                    }
                }
                continue;
            }
            if (entity.getFaction() != null && entity.getFaction().getId().equals(Factions.PLAYER) &&
                    entity.isFreeTransfer()) {
                if (entity.getCargo() == cargoCopy) {
                    continue;
                }
                if (entity.getCargo() != null) {
                    allStorage.put(entity, entity.getCargo());
                }
            }
        }

        for (Map.Entry<Object, CargoAPI> entry : allStorage.entrySet()) {
            CargoAPI cargo = entry.getValue();
            for (CargoStackAPI stack : cargo.getStacksCopy()) {
                cargoCopy.addFromStack(stack);
            }
        }

        String data = "";

        data += "{X}" + playerStats.getXP();

        for (String skillId : Global.getSettings().getSkillIds()) {
            SkillSpecAPI spec = Global.getSettings().getSkillSpec(skillId);
            if (spec.isAptitudeEffect()) {
                String aptitudeId = skillId.replace("aptitude_", "");
                if (!Global.getSettings().getAptitudeIds().contains(aptitudeId)) {
                    continue;
                }

                if (playerStats.getAptitudeLevel(aptitudeId) > 0) {
                    data += "{SA|" + aptitudeId + "}" + playerStats.getAptitudeLevel(aptitudeId);
                }
            } else {
                if (playerStats.getSkillLevel(skillId) > 0) {
                    data += "{SS|" + skillId + "}" + playerStats.getSkillLevel(skillId);
                }
            }
        }

        for (String modId : Global.getSector().getCharacterData().getHullMods()) {
            data += "{H}" + modId;
        }

        for (FactionAPI faction : Global.getSector().getAllFactions()) {
            if (faction.getId().equals(Factions.PLAYER) || faction.getId().equals(Factions.NEUTRAL)) {
                continue;
            }

            data += "{F|" + faction.getId() + '}' + faction.getRelationship(Factions.PLAYER);
        }

        if (playerFleet.getContainingLocation() instanceof StarSystemAPI) {
            data += "{L|" + playerFleet.getLocation().x + '|' + playerFleet.getLocation().y + '}' +
            ((StarSystemAPI) playerFleet.getContainingLocation()).getBaseName();
        } else {
            data += "{L|" + playerFleet.getLocation().x + '|' + playerFleet.getLocation().y + '}' + "hyperspace";
        }

        boolean dsExists = Global.getSettings().getModManager().isModEnabled("dynasector");
        if (dsExists) {
            Float bountyLevel = (Float) Global.getSector().getPersistentData().get("ds_personBountyLevel");
            if (bountyLevel != null) {
                data += "{B}" + bountyLevel;
            }
        } else {
            // See if vanilla exists
            Object sharedData = Global.getSector().getPersistentData().get(CoreScript.SHARED_DATA_KEY);
            if (sharedData != null) {
                data += "{B}" + SharedData.getData().getPersonBountyEventData().getLevel();
            }
        }

        boolean swpExists = Global.getSettings().getModManager().isModEnabled("swp");
        if (swpExists) {
            SWP_IBBTracker tracker = SWP_IBBTracker.getTracker();
            if (tracker != null && !tracker.noStagesComplete()) {
                data += "{FB";
                for (FamousBountyStage stage : FamousBountyStage.values()) {
                    if (tracker.isStageComplete(stage)) {
                        data += "|" + stage.name();
                    }
                }
                data += "}";
            }
        }

        boolean uwExists = Global.getSettings().getModManager().isModEnabled("underworld");
        if (uwExists) {
            Integer dickersonLevel = (Integer) Global.getSector().getPersistentData().get("uw_dickerson_level");
            if (dickersonLevel != null) {
                data += "{DL}" + dickersonLevel;
            }
        }

        // Transfer all the ships and weapons piecemeal; don't even try to make a variant structure
        List<FleetMemberAPI> members = playerFleetData.getMembersListCopy();
        for (Map.Entry<Object, CargoAPI> entry : allStorage.entrySet()) {
            CargoAPI storage = entry.getValue();
            members.addAll(storage.getMothballedShips().getMembersListCopy());
        }

        cargoCopy.sort();

        Map<FleetMemberAPI, Integer> memberIdMap = new HashMap<>(members.size());
        int memberIdIndex = 0;
        for (FleetMemberAPI member : members) {
            if (member.isFighterWing()) {
                data += "{M|F|" + member.getSpecId() + "|" + member.getRepairTracker().getBaseCR() + '}';
            } else {
                data += "{M|S|" + member.getHullId() + "|" + member.getRepairTracker().getBaseCR() + "|" +
                member.getStatus().getHullFraction() + "|" +
                member.getRepairTracker().computeRepairednessFraction() + "|" + memberIdIndex + '}' + stripMagicChars(
                        member.getShipName());

                ShipVariantAPI variant = member.getVariant();
                for (String slot : variant.getNonBuiltInWeaponSlots()) {
                    String weaponId = variant.getWeaponId(slot);
                    cargoCopy.addWeapons(weaponId, 1);
                }
                for (String wingId : variant.getNonBuiltInWings()) {
                    cargoCopy.addFighters(wingId, 1);
                }
                for (String moduleSlot : variant.getStationModules().keySet()) {
                    ShipVariantAPI module = member.getModuleVariant(moduleSlot);
                    if (module == null) {
                        continue;
                    }

                    for (String slot : module.getNonBuiltInWeaponSlots()) {
                        String weaponId = module.getWeaponId(slot);
                        cargoCopy.addWeapons(weaponId, 1);
                    }
                    for (String wingId : module.getNonBuiltInWings()) {
                        cargoCopy.addFighters(wingId, 1);
                    }
                }

                cargoCopy.sort();

                memberIdMap.put(member, memberIdIndex);
                memberIdIndex++;
            }
        }

        cargoCopy.sort();

        data += "{C}" + playerCargo.getCredits().get();

        for (CargoStackAPI stack : cargoCopy.getStacksCopy()) {
            if (stack.isCommodityStack()) {
                String stackCommodity = stack.getCommodityId();
                data += "{S|R|" + stackCommodity + '}' + stack.getSize();
            } else if (stack.isCrewStack()) {
                data += "{S|C}" + (int) stack.getSize();
            } else if (stack.isMarineStack()) {
                data += "{S|M}" + (int) stack.getSize();
            } else if (stack.isWeaponStack()) {
                WeaponSpecAPI stackWeapon = stack.getWeaponSpecIfWeapon();
                data += "{S|W|" + stackWeapon.getWeaponId() + '}' + (int) stack.getSize();
            } else if (stack.isFighterWingStack()) {
                FighterWingSpecAPI stackWing = stack.getFighterWingSpecIfWing();
                data += "{S|F|" + stackWing.getId() + '}' + (int) stack.getSize();
            } else if (stack.isModSpecStack()) {
                HullModSpecAPI stackMod = stack.getHullModSpecIfHullMod();
                data += "{S|H|" + stackMod.getId() + '}' + (int) stack.getSize();
            }
        }

        cargoCopy.clear();

        for (OfficerDataAPI officerData : playerFleetData.getOfficersCopy()) {
            PersonAPI officer = officerData.getPerson();
            FleetMemberAPI member = playerFleetData.getMemberWithCaptain(officer);
            int memberId = -1;
            if (member != null && memberIdMap.containsKey(member)) {
                memberId = memberIdMap.get(member);
            }

            data += "{O|" + stripMagicChars(officer.getName().getFirst()) + "|" + stripMagicChars(
            officer.getName().getLast()) + "|" +
            officer.getName().getGender().name() + "|" + officer.getPortraitSprite() + "|" + officer.getRankId() + "|" +
            officer.getPostId() + "|" +
            officer.getPersonalityAPI().getId() + "|" + memberId;
            for (String skillId : Global.getSettings().getSkillIds()) {
                if (officer.getStats().getSkillLevel(skillId) > 0) {
                    data += "|" + skillId + "\\" + officer.getStats().getSkillLevel(skillId);
                }
            }
            data += "}" + officer.getStats().getXP();
        }

        Map<ShipVariantAPI, Integer> moduleIdMap = new HashMap<>(10);
        int moduleIdIndex = 0;
        for (FleetMemberAPI member : members) {
            if (member.isFighterWing()) {
                continue;
            }
            ShipVariantAPI variant = member.getVariant();
            if (variant == null || variant.isEmptyHullVariant()) {
                continue;
            }
            if (!memberIdMap.containsKey(member)) {
                continue;
            }
            int memberId = memberIdMap.get(member);

            data += "{V|" + memberId + "|";
            boolean first = true;
            for (String slot : variant.getNonBuiltInWeaponSlots()) {
                String weapon = variant.getWeaponId(slot);
                if (weapon == null) {
                    continue;
                }
                if (!first) {
                    data += "\\";
                }
                data += slot + ":" + weapon;
                first = false;
            }
            data += "|";
            first = true;
            for (String slot : variant.getStationModules().keySet()) {
                if (member.getModuleVariant(slot) == null) {
                    continue;
                }
                if (!first) {
                    data += "\\";
                }
                data += slot + ":" + moduleIdIndex;
                moduleIdMap.put(member.getModuleVariant(slot), moduleIdIndex);
                moduleIdIndex++;
                first = false;
            }
            data += "|";
            first = true;
            for (String wing : variant.getNonBuiltInWings()) {
                if (!first) {
                    data += "\\";
                }
                data += wing;
                first = false;
            }
            data += "|";
            first = true;
            Collection<String> builtIn = getBuiltInHullMods(member.getVariant());
            for (String mod : variant.getHullMods()) {
                if (builtIn.contains(mod)) {
                    continue;
                }
                if (!first) {
                    data += "\\";
                }
                data += mod;
                first = false;
            }
            data += "|";
            first = true;
            for (String mod : variant.getPermaMods()) {
                if (!first) {
                    data += "\\";
                }
                data += mod;
                first = false;
            }
            data += "|";
            first = true;
            for (WeaponGroupSpec group : variant.getWeaponGroups()) {
                if (group.getSlots().isEmpty()) {
                    continue;
                }
                if (!first) {
                    data += "\\";
                }
                if (group.isAutofireOnByDefault()) {
                    data += 1;
                } else {
                    data += 0;
                }
                if (group.getType() == WeaponGroupType.ALTERNATING) {
                    data += ":" + 1;
                } else {
                    data += ":" + 0;
                }
                for (String slot : group.getSlots()) {
                    data += ":" + slot;
                }
                first = false;
            }
            data += "|" + variant.getNumFluxVents() + "|" + variant.getNumFluxCapacitors() + "}" + stripMagicChars(
            variant.getDisplayName());
        }

        for (ShipVariantAPI module : moduleIdMap.keySet()) {
            int moduleId = moduleIdMap.get(module);

            data += "{MV|" + moduleId + "|";
            boolean first = true;
            for (String slot : module.getNonBuiltInWeaponSlots()) {
                String weapon = module.getWeaponId(slot);
                if (weapon == null) {
                    continue;
                }
                if (!first) {
                    data += "\\";
                }
                data += slot + ":" + weapon;
                first = false;
            }
            data += "|";
            first = true;
            for (String wing : module.getNonBuiltInWings()) {
                if (!first) {
                    data += "\\";
                }
                data += wing;
                first = false;
            }
            data += "|";
            first = true;
            Collection<String> builtIn = getBuiltInHullMods(module);
            for (String mod : module.getHullMods()) {
                if (builtIn.contains(mod)) {
                    continue;
                }
                if (!first) {
                    data += "\\";
                }
                data += mod;
                first = false;
            }
            data += "|";
            first = true;
            for (String mod : module.getPermaMods()) {
                if (!first) {
                    data += "\\";
                }
                data += mod;
                first = false;
            }
            data += "|";
            first = true;
            for (WeaponGroupSpec group : module.getWeaponGroups()) {
                if (group.getSlots().isEmpty()) {
                    continue;
                }
                if (!first) {
                    data += "\\";
                }
                if (group.isAutofireOnByDefault()) {
                    data += 1;
                } else {
                    data += 0;
                }
                if (group.getType() == WeaponGroupType.ALTERNATING) {
                    data += ":" + 1;
                } else {
                    data += ":" + 0;
                }
                for (String slot : group.getSlots()) {
                    data += ":" + slot;
                }
                first = false;
            }
            data += "|" + module.getNumFluxVents() + "|" + module.getNumFluxCapacitors() + "}" + stripMagicChars(
            module.getDisplayName());
        }

        if (nexerelinExists) {
            String playerAlignedFactionId = PlayerFactionStore.getPlayerFactionId();
            for (MarketAPI market : ExerelinUtilsFaction.getFactionMarkets(playerAlignedFactionId)) {
                data += "{NPM}" + market.getId();
            }

            List<FactionAPI> allFactions = Global.getSector().getAllFactions();
            for (FactionAPI faction : allFactions) {
                String id = faction.getId();
                if (id.contentEquals(playerAlignedFactionId)) {
                    continue;
                }

                for (MarketAPI market : ExerelinUtilsFaction.getFactionMarkets(id)) {
                    data += "{NM|" + id + "}" + market.getId();
                }
            }

            Set<String> pairsDone = new HashSet<>(allFactions.size() * allFactions.size() / 2);
            for (FactionAPI faction : allFactions) {
                String id = faction.getId();
                if (id.contentEquals(playerAlignedFactionId) || faction.isNeutralFaction() || faction.isPlayerFaction() ||
                        (ExerelinConfig.getExerelinFactionConfig(id) == null)) {
                    continue;
                }

                for (FactionAPI otherFaction : allFactions) {
                    String otherId = otherFaction.getId();
                    if (otherId.contentEquals(id) || otherId.contentEquals(playerAlignedFactionId) ||
                            otherFaction.isNeutralFaction() ||
                            otherFaction.isPlayerFaction() || (ExerelinConfig.getExerelinFactionConfig(otherId) == null)) {
                        continue;
                    }

                    String pairStrA = id + "~" + otherId;
                    String pairStrB = otherId + "~" + id;
                    if (pairsDone.contains(pairStrA) || pairsDone.contains(pairStrB)) {
                        continue;
                    }

                    data += "{NF|" + id + "|" + otherId + "}" + faction.getRelationship(otherId);
                    pairsDone.add(pairStrA);
                }
            }

            for (Alliance alliance : AllianceManager.getAllianceList()) {
                if (alliance.getMembersCopy().size() < 2) {
                    continue;
                }

                data += "{NA|" + alliance.getAlignment().name() + "|";

                boolean first = true;
                for (String member : alliance.getMembersCopy()) {
                    if (!first) {
                        data += "\\";
                    }
                    data += member;
                    first = false;
                }

                data += "}" + alliance.getName();
            }
        }

        if (omnifactory != null) {
            for (CargoStackAPI stack : omnifactory.getStacksCopy()) {
                if (stack.isWeaponStack()) {
                    WeaponSpecAPI stackWeapon = stack.getWeaponSpecIfWeapon();
                    data += "{OF|W|" + stackWeapon.getWeaponId() + '}' + (int) stack.getSize();
                }
            }
            for (FleetMemberAPI member : omnifactory.getMothballedShips().getMembersListCopy()) {
                if (member.isFighterWing()) {
                    data += "{OF|F|" + member.getSpecId() + '}';
                } else {
                    data += "{OF|S|" + member.getHullId() + '}' + stripMagicChars(member.getShipName());
                }
            }
        }

        return data;
    }

    private static String stripMagicChars(String str) {
        String out = str;
        for (char magicChar : MAGIC_CHARS) {
            out = out.replace(Character.toString(magicChar), "");
        }
        return out;
    }

    @Override
    public CommandResult runCommand(String args, CommandContext context) {
        if (context != CommandContext.CAMPAIGN_MAP) {
            Console.showMessage(CommonStrings.ERROR_CAMPAIGN_ONLY);
            return CommandResult.WRONG_CONTEXT;
        }

        String data = serializePlayerData();

        String[] argList = args.split("\\s");
        Set<String> filteredArgs = new HashSet<>(argList.length);
        CommandResult ret = null;
        for (String arg : argList) {
            String trim = arg.trim().toLowerCase();
            if (trim.isEmpty()) {
                continue;
            }

            filteredArgs.add(trim);
            switch (trim) {
                case ARG_UNCOMPRESSED:
                    break;
                case ARG_HELP:
                    ret = CommandResult.SUCCESS;
                    break;
                default:
                    ret = CommandResult.BAD_SYNTAX;
                    break;
            }
            if (ret != null) {
                Console.showMessage(
                        "Available flags:\n" +
                        ARG_UNCOMPRESSED + " : Save uncompressed data\n" +
                        ARG_HELP + " : Show these options");
                return ret;
            }
        }

        try {
            if (!filteredArgs.contains(ARG_UNCOMPRESSED)) {
                data = compressData(data);
            }
        } catch (Exception ex) {
            Console.showMessage("Data compression failed!");
            return CommandResult.ERROR;
        }

        StringSelection stringSelection = new StringSelection(data);
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);

        Console.showMessage("Save data copied to the clipboard.");
        return CommandResult.SUCCESS;
    }
}
