package org.dark.transfer.console.commands;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoAPI.CargoItemType;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.CommoditySpecAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.characters.FullName.Gender;
import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.characters.SkillSpecAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.impl.campaign.CoreScript;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import com.fs.starfarer.api.loading.FighterWingSpecAPI;
import com.fs.starfarer.api.loading.VariantSource;
import com.fs.starfarer.api.loading.WeaponGroupSpec;
import com.fs.starfarer.api.loading.WeaponGroupType;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import com.fs.starfarer.api.util.Misc;
import data.scripts.campaign.events.SWP_IBBTracker;
import data.scripts.campaign.missions.SWP_FamousBountyEvent.FamousBountyStage;
import exerelin.campaign.AllianceManager;
import exerelin.campaign.PlayerFactionStore;
import exerelin.campaign.SectorManager;
import exerelin.campaign.alliances.Alliance;
import exerelin.campaign.alliances.Alliance.Alignment;
import exerelin.utilities.ExerelinUtilsReputation;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.Inflater;
import org.apache.commons.codec.binary.Base64;
import org.lazywizard.console.BaseCommand;
import org.lazywizard.console.CommonStrings;
import org.lazywizard.console.Console;
import org.lazywizard.console.commands.Storage;

import static org.dark.transfer.console.commands.CopySave.BLOCK_SIZE;

public class LoadSave implements BaseCommand {

    private static final String ARG_HELP = "-help";
    private static final String ARG_NO_BOUNTIES = "-no-bounties";
    private static final String ARG_NO_CARGO = "-no-cargo";
    private static final String ARG_NO_CREDITS = "-no-credits";
    private static final String ARG_NO_EXERELIN_ALLIANCES = "-no-exerelin-alliances";
    private static final String ARG_NO_EXERELIN_MARKETS = "-no-exerelin-markets";
    private static final String ARG_NO_EXERELIN_REP = "-no-exerelin-rep";
    private static final String ARG_NO_FLEET = "-no-fleet";
    private static final String ARG_NO_HULLMODS = "-no-mods";
    private static final String ARG_NO_LOCATION = "-no-location";
    private static final String ARG_NO_OFFICERS = "-no-officers";
    private static final String ARG_NO_REP = "-no-rep";
    private static final String ARG_NO_SKILLS = "-no-skills";
    private static final String ARG_NO_VARIANTS = "-no-variants";
    private static final String ARG_NO_XP = "-no-xp";
    private static final String ARG_UNCOMPRESSED = "-uncompressed";

    private static final int SUCCESSES_PER_BOUNTY_LEVEL = 3;

    private static String decompressData(String input) throws Exception {
        byte[] data = input.getBytes("US-ASCII");
        data = Base64.decodeBase64(data);

        List<byte[]> fullOutput = new ArrayList<>(10);
        byte[] result = new byte[BLOCK_SIZE];
        Inflater decompresser = new Inflater();
        decompresser.setInput(data);
        int resultLength;
        while (true) {
            result = new byte[BLOCK_SIZE];
            resultLength = decompresser.inflate(result);
            if (resultLength == BLOCK_SIZE) {
                fullOutput.add(result);
            } else {
                break;
            }
        }
        decompresser.end();

        String output = new String();
        for (byte[] bytes : fullOutput) {
            output += new String(bytes, "UTF-8");
        }
        output += new String(result, 0, resultLength, "UTF-8");

        return output;
    }

    // Naturally, we can't use actual serialization, so it's gotta be done manually
    @SuppressWarnings(value = "unchecked")
    private static boolean deserializePlayerData(final String data, final Set<String> args) {
        boolean nexerelinExists = Global.getSettings().getModManager().isModEnabled("nexerelin");
        CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
        MutableCharacterStatsAPI playerStats = playerFleet.getCommander().getStats();
        CargoAPI storage = Storage.retrieveStorage();
        CargoAPI playerCargo = playerFleet.getCargo();
        FleetDataAPI playerFleetData = playerFleet.getFleetData();
        storage.clear();
        CargoAPI omnifactory = null;
        List<SectorEntityToken> entities = new ArrayList<>(Global.getSector().getHyperspace().getEntities(
                                SectorEntityToken.class));
        for (StarSystemAPI system : Global.getSector().getStarSystems()) {
            entities.addAll(system.getEntities(SectorEntityToken.class));
        }
        for (SectorEntityToken entity : entities) {
            if (entity == playerFleet) {
                continue;
            }
            MarketAPI market = entity.getMarket();
            if (market != null) {
                for (SubmarketAPI submarket : market.getSubmarketsCopy()) {
                    if (submarket.getFaction().getId().equals(Factions.PLAYER) && submarket.getPlugin().isFreeTransfer()) {
                        submarket.getCargo().clear();
                    }
                    if (submarket.getSpecId().contentEquals("omnifac_market")) {
                        omnifactory = submarket.getCargo();
                    }
                }
            }
            if (entity.getFaction() != null && entity.getFaction().getId().equals(Factions.PLAYER) &&
                    entity.isFreeTransfer()) {
                if (entity.getCargo() != null) {
                    entity.getCargo().clear();
                }
            }
        }
        int index, endIndex;

        if (!args.contains(ARG_NO_XP)) {
            index = data.indexOf("{X}");
            if (index < 0) {
                return false;
            } else {
                index += 3;
            }
            endIndex = data.indexOf('{', index);
            if (endIndex == -1) {
                endIndex = data.length();
            }
            long playerXP = Long.parseLong(data.substring(index, endIndex));
            playerXP = Math.min(playerXP, Global.getSettings().getLevelupPlugin().getXPForLevel(
                                Global.getSettings().getLevelupPlugin().getMaxLevel()));
            long toAdd = playerXP - playerStats.getXP();
            if (toAdd > 0) {
                playerStats.addXP(toAdd);
                playerStats.levelUpIfNeeded();
            }
        }

        if (!args.contains(ARG_NO_SKILLS)) {
            index = 0;
            while (true) {
                index = data.indexOf("{SA|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 4;
                }
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                String aptitudeId = data.substring(index, endIndex);
                if (Global.getSettings().getAptitudeIds().contains(aptitudeId)) {
                    index = endIndex + 1;
                    endIndex = data.indexOf('{', index);
                    if (endIndex == -1) {
                        endIndex = data.length();
                    }
                    int aptitudeLevel = (int) Float.parseFloat(data.substring(index, endIndex));
                    int ptsNeeded = Math.max(0, aptitudeLevel - Math.round(playerStats.getAptitudeLevel(aptitudeId)));
                    ptsNeeded = Math.max(0, ptsNeeded - Math.max(0, ptsNeeded - playerStats.getPoints()));
                    if (ptsNeeded > 0) {
                        playerStats.addPoints(-ptsNeeded);
                        playerStats.setAptitudeLevel(aptitudeId, aptitudeLevel);
                    }
                }
            }

            index = 0;
            while (true) {
                index = data.indexOf("{SS|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 4;
                }
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                String skillId = data.substring(index, endIndex);
                if (Global.getSettings().getSkillIds().contains(skillId)) {
                    index = endIndex + 1;
                    endIndex = data.indexOf('{', index);
                    if (endIndex == -1) {
                        endIndex = data.length();
                    }
                    int skillLevel = (int) Float.parseFloat(data.substring(index, endIndex));
                    int ptsNeeded = Math.max(0, skillLevel - Math.round(playerStats.getSkillLevel(skillId)));
                    ptsNeeded = Math.max(0, ptsNeeded - Math.max(0, ptsNeeded - playerStats.getPoints()));
                    if (ptsNeeded > 0) {
                        playerStats.addPoints(-ptsNeeded);
                        playerStats.setSkillLevel(skillId, skillLevel);
                    }
                }
            }
        }

        if (!args.contains(ARG_NO_HULLMODS)) {
            index = 0;
            while (true) {
                index = data.indexOf("{H}", index);
                if (index < 0) {
                    break;
                } else {
                    index += 3;
                }
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                String modId = data.substring(index, endIndex);
                try {
                    Global.getSettings().getHullModSpec(modId);
                } catch (RuntimeException ex) {
                    continue;
                }
                Global.getSector().getCharacterData().addHullMod(modId);
            }
        }

        boolean swpExists = Global.getSettings().getModManager().isModEnabled("swp");
        boolean uwExists = Global.getSettings().getModManager().isModEnabled("underworld");
        if (!args.contains(ARG_NO_BOUNTIES)) {
            index = data.indexOf("{B}");
            if (index >= 0) {
                index += 3;
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                if (swpExists) {
                    float bountyLevel = Float.parseFloat(data.substring(index, endIndex));
                    if (bountyLevel > 0f) {
                        Global.getSector().getPersistentData().put("ds_personBountyLevel", bountyLevel);
                    }
                } else {
                    // See if vanilla exists
                    Object sharedData = Global.getSector().getPersistentData().get(CoreScript.SHARED_DATA_KEY);
                    if (sharedData != null) {
                        int bountyLevel = Integer.parseInt(data.substring(index, endIndex)) -
                            SharedData.getData().getPersonBountyEventData().getLevel();
                        if (bountyLevel > 0) {
                            for (int i = 0; i < bountyLevel * SUCCESSES_PER_BOUNTY_LEVEL; i++) {
                                SharedData.getData().getPersonBountyEventData().reportSuccess();
                            }
                        }
                    }
                }
            }

            if (swpExists) {
                SWP_IBBTracker tracker = SWP_IBBTracker.getTracker();
                if (tracker != null) {
                    index = data.indexOf("{FB|", index);
                    if (index >= 0) {
                        index += 4;
                        endIndex = data.indexOf('}', index);
                        if (endIndex == -1) {
                            return false;
                        }

                        String stageNamePPConcat = data.substring(index, endIndex);
                        String allStageNames[] = stageNamePPConcat.split("\\|");

                        for (String stageName : allStageNames) {
                            FamousBountyStage stage;
                            try {
                                stage = FamousBountyStage.valueOf(stageName);
                            } catch (IllegalArgumentException ex) {
                                stage = null;
                            }

                            if (stage != null) {
                                tracker.reportStageCompleted(stage);
                            }
                        }
                    }
                }
            }

            if (uwExists) {
                index = data.indexOf("{DL}");
                if (index >= 0) {
                    index += 4;
                    endIndex = data.indexOf('{', index);
                    if (endIndex == -1) {
                        endIndex = data.length();
                    }
                    int dickersonLevel = Integer.parseInt(data.substring(index, endIndex));
                    Global.getSector().getPersistentData().put("uw_dickerson_level", dickersonLevel);
                }
            }
        }
        if (!args.contains(ARG_NO_REP)) {
            index = 0;
            while (true) {
                index = data.indexOf("{F|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 3;
                }
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                FactionAPI faction = Global.getSector().getFaction(data.substring(index, endIndex));
                if (faction != null) {
                    index = endIndex + 1;
                    endIndex = data.indexOf('{', index);
                    if (endIndex == -1) {
                        endIndex = data.length();
                    }
                    float relationship = Float.parseFloat(data.substring(index, endIndex));
                    faction.setRelationship(Factions.PLAYER, relationship);
                }
            }
            if (nexerelinExists) {
                ExerelinUtilsReputation.syncFactionRelationshipsToPlayer();
            }
        }
        Map<Integer, FleetMemberAPI> memberIdMap = null;
        if (!args.contains(ARG_NO_FLEET)) {
            playerFleetData.clear();

            memberIdMap = new HashMap<>(25);
            index = 0;
            while (true) {
                index = data.indexOf("{M|S|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 5;
                }
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                String hullId = data.substring(index, endIndex);

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                float CR = Float.parseFloat(data.substring(index, endIndex));

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                float hullFraction = Float.parseFloat(data.substring(index, endIndex));

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                float repairedness = Float.parseFloat(data.substring(index, endIndex));

                index = endIndex + 1;
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                int memberId = Integer.parseInt(data.substring(index, endIndex));

                index = endIndex + 1;
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                String shipName = data.substring(index, endIndex);

                FleetMemberAPI member;
                try {
                    if (hullId.endsWith(Misc.D_HULL_SUFFIX)) {
                        member = Global.getFactory().createFleetMember(FleetMemberType.SHIP,
                                                                       hullId.replace(Misc.D_HULL_SUFFIX, "") + "_Hull");
                        ShipVariantAPI variant = member.getVariant();
                        if (!variant.getHullSpec().isDHull()) {
                            ShipHullSpecAPI dHull = Global.getSettings().getHullSpec(hullId);
                            variant.setHullSpecAPI(dHull);
                        }
                    } else {
                        member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, hullId + "_Hull");
                    }
                } catch (RuntimeException ex) {
                    continue;
                }
                if (repairedness < 1f) {
                    member.getStatus().disable();
                    member.getStatus().repairDisabledABit();
                    member.getStatus().setHullFraction(hullFraction);
                    for (int i = 1; i < member.getStatus().getNumStatuses(); i++) {
                        if (Math.random() > hullFraction) {
                            member.getStatus().setDetached(i, true);
                        } else {
                            member.getStatus().setDetached(i, false);
                            member.getStatus().setHullFraction(i, hullFraction);
                        }
                    }
                    float currentRepairednessFraction = member.getRepairTracker().computeRepairednessFraction();
                    if (repairedness - currentRepairednessFraction > 0f) {
                        member.getRepairTracker().performRepairsFraction(repairedness - currentRepairednessFraction);
                    }
                }
                member.setShipName(shipName);
                if (playerFleetData.getNumMembers() < Global.getSettings().getMaxShipsInFleet()) {
                    member.getRepairTracker().setCR(CR);
                    playerFleetData.addFleetMember(member);
                    playerFleetData.syncMemberLists();
                } else {
                    storage.getMothballedShips().addFleetMember(member);
                }
                memberIdMap.put(memberId, member);
            }

            index = 0;
            while (true) {
                index = data.indexOf("{M|F|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 5;
                }
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                String wingId = data.substring(index, endIndex);
                index = endIndex + 1;
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                float CR = Float.parseFloat(data.substring(index, endIndex));
                FleetMemberAPI member;
                try {
                    member = Global.getFactory().createFleetMember(FleetMemberType.FIGHTER_WING, wingId);
                } catch (RuntimeException ex) {
                    continue;
                }
                member.getRepairTracker().setCR(CR);
                playerFleetData.addFleetMember(member);
            }

            if (playerFleetData.getMembersListCopy().isEmpty()) {
                playerFleetData.addFleetMember(Global.getFactory().createFleetMember(FleetMemberType.SHIP,
                                                                                     "cerberus_Hull"));
            }
        }
        if (!args.contains(ARG_NO_LOCATION)) {
            index = 0;
            while (true) {
                index = data.indexOf("{L|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 3;
                }
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                float x = Float.parseFloat(data.substring(index, endIndex));

                index = endIndex + 1;
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                float y = Float.parseFloat(data.substring(index, endIndex));

                index = endIndex + 1;
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                String system = data.substring(index, endIndex);

                LocationAPI location;
                if (system.contentEquals("hyperspace")) {
                    location = Global.getSector().getHyperspace();
                } else {
                    try {
                        location = Global.getSector().getStarSystem(system);
                    } catch (RuntimeException ex) {
                        location = Global.getSector().getHyperspace();
                    }
                    if (location == null) {
                        location = Global.getSector().getHyperspace();
                    }
                }
                playerFleet.getContainingLocation().removeEntity(playerFleet);
                location.addEntity(playerFleet);
                Global.getSector().setCurrentLocation(location);
                playerFleet.setLocation(x, y);
            }
        }
        if (!args.contains(ARG_NO_CARGO)) {
            playerCargo.clear();
        }
        if (!args.contains(ARG_NO_CREDITS)) {
            index = data.indexOf("{C}");
            if (index < 0) {
                return false;
            } else {
                index += 3;
            }
            endIndex = data.indexOf('{', index);
            if (endIndex == -1) {
                endIndex = data.length();
            }
            float playerCredits = Float.parseFloat(data.substring(index, endIndex));
            if (playerCredits > 0) {
                playerCargo.getCredits().set(playerCredits);
            }
        }
        if (!args.contains(ARG_NO_CARGO)) {
            index = 0;
            while (true) {
                index = data.indexOf("{S|R|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 5;
                }
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                String commodity = data.substring(index, endIndex);
                CommoditySpecAPI spec;
                try {
                    spec = Global.getSector().getEconomy().getCommoditySpec(commodity);
                } catch (RuntimeException ex) {
                    continue;
                }
                if (spec == null) {
                    continue;
                }
                index = endIndex + 1;
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                float size = Float.parseFloat(data.substring(index, endIndex));
                if (commodity.endsWith("crew") || commodity.contentEquals("supplies") ||
                        commodity.contentEquals("fuel")) {
                    playerCargo.addCommodity(commodity, size);
                } else {
                    storage.addCommodity(commodity, size);
                }
            }

            index = 0;
            while (true) {
                index = data.indexOf("{S|C}", index);
                if (index < 0) {
                    break;
                } else {
                    index += 5;
                }
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                int size = Integer.parseInt(data.substring(index, endIndex));
                playerCargo.addCrew(size);
            }

            index = 0;
            while (true) {
                index = data.indexOf("{S|M}", index);
                if (index < 0) {
                    break;
                } else {
                    index += 5;
                }
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                int size = Integer.parseInt(data.substring(index, endIndex));
                storage.addMarines(size);
            }

            index = 0;
            while (true) {
                index = data.indexOf("{S|H|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 5;
                }
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                String modId = data.substring(index, endIndex);
                try {
                    Global.getSettings().getHullModSpec(modId);
                } catch (RuntimeException ex) {
                    continue;
                }
                index = endIndex + 1;
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                int size = Integer.parseInt(data.substring(index, endIndex));
                storage.addHullmods(modId, size);
            }

            storage.sort();
            playerCargo.sort();
        }
        if (!args.contains(ARG_NO_CARGO) || !args.contains(ARG_NO_VARIANTS)) {
            index = 0;
            while (true) {
                index = data.indexOf("{S|W|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 5;
                }
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                String weaponId = data.substring(index, endIndex);
                try {
                    Global.getSettings().getWeaponSpec(weaponId);
                } catch (RuntimeException ex) {
                    continue;
                }
                index = endIndex + 1;
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                int size = Integer.parseInt(data.substring(index, endIndex));
                storage.addWeapons(weaponId, size);
            }

            index = 0;
            while (true) {
                index = data.indexOf("{S|F|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 5;
                }
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                String windId = data.substring(index, endIndex);
                try {
                    Global.getSettings().getFighterWingSpec(windId);
                } catch (RuntimeException ex) {
                    continue;
                }
                index = endIndex + 1;
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                int size = Integer.parseInt(data.substring(index, endIndex));
                storage.addFighters(windId, size);
            }

            storage.sort();
        }
        if (!args.contains(ARG_NO_OFFICERS)) {
            for (OfficerDataAPI officerData : playerFleetData.getOfficersCopy()) {
                playerFleetData.removeOfficer(officerData.getPerson());
            }

            index = 0;
            while (true) {
                index = data.indexOf("{O|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 3;
                }

                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                String firstName = data.substring(index, endIndex);

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                String lastName = data.substring(index, endIndex);

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                Gender gender;
                try {
                    gender = Gender.valueOf(data.substring(index, endIndex));
                } catch (IllegalArgumentException ex) {
                    return false;
                }

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                String portraitSprite = data.substring(index, endIndex);
                SpriteAPI portrait;
                try {
                    portrait = Global.getSettings().getSprite(portraitSprite);
                    if (portrait.getTextureId() == 0) {
                        portrait = null;
                    }
                } catch (Exception ex) {
                    portrait = null; // just use any old portrait
                }

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                String rankId = data.substring(index, endIndex);

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                String postId = data.substring(index, endIndex);

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                String personalityId = data.substring(index, endIndex);

                boolean hasSkills;
                index = endIndex + 1;
                endIndex = Math.min(data.indexOf('|', index), data.indexOf('}', index));
                if (endIndex == -1) {
                    hasSkills = false;
                    endIndex = data.indexOf('}', index);
                } else {
                    hasSkills = (data.indexOf('|', index) == endIndex);
                }
                if (endIndex == -1) {
                    return false;
                }
                int memberId = Integer.parseInt(data.substring(index, endIndex));

                int increasesPerLevel = Math.round(Global.getSettings().getFloat("officerSkillIncreasePerLevel"));
                List<OfficerSkillPick> officerSkills = new ArrayList<>(9);
                if (hasSkills) {
                    while (true) {
                        index = endIndex + 1;
                        endIndex = data.indexOf('\\', index);
                        if (endIndex == -1) {
                            return false;
                        }
                        String skillId = data.substring(index, endIndex);

                        index = endIndex + 1;
                        endIndex = Math.min(data.indexOf('|', index), data.indexOf('}', index));
                        if (endIndex == -1) {
                            endIndex = data.indexOf('}', index);
                            if (endIndex == -1) {
                                return false;
                            } else {
                                break;
                            }
                        }
                        int skillLevel = (int) Float.parseFloat(data.substring(index, endIndex));

                        if (Global.getSettings().getSkillIds().contains(skillId)) {
                            SkillSpecAPI skill = Global.getSettings().getSkillSpec(skillId);
                            if (!skill.isCombatOfficerSkill()) {
                                continue;
                            }
                        }
                        officerSkills.add(new OfficerSkillPick(skillId, skillLevel));

                        if (data.charAt(endIndex) == '}') {
                            break;
                        }
                    }
                }

                index = endIndex + 1;
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                long xp = Long.parseLong(data.substring(index, endIndex));

                PersonAPI officer;
                OfficerDataAPI officerData;
                if (!officerSkills.isEmpty()) {
                    officer = Global.getSector().getFaction(Factions.PLAYER).createRandomPerson();
                    officerData = Global.getFactory().createOfficerData(officer);
                    for (int i = 0; i < 2; i++) {
                        if (officerSkills.isEmpty()) {
                            break;
                        }
                        officer.getStats().increaseSkill(officerSkills.get(0).skillId);
                        officerSkills.get(0).level -= increasesPerLevel;
                        if (officerSkills.get(0).level <= 0) {
                            officerSkills.remove(0);
                        }
                    }
                } else {
                    officer =
                    OfficerManagerEvent.createOfficer(Global.getSector().getFaction(Factions.PLAYER), 1, false);
                    officerData = Global.getFactory().createOfficerData(officer);
                }

                officer.getName().setFirst(firstName);
                officer.getName().setLast(lastName);
                officer.getName().setGender(gender);
                if (portrait != null) {
                    officer.setPortraitSprite(portraitSprite);
                }
                officer.setRankId(rankId);
                officer.setPostId(postId);
                officer.setPersonality(personalityId);

                playerFleetData.addOfficer(officer);
                officerData.addXP(xp);
                while (officerData.canLevelUp() && !officerSkills.isEmpty()) {
                    officerData.levelUp(officerSkills.get(0).skillId);
                    officerSkills.get(0).level -= increasesPerLevel;
                    if (officerSkills.get(0).level <= 0) {
                        officerSkills.remove(0);
                    }
                }

                if (memberId >= 0) {
                    FleetMemberAPI member = (memberIdMap != null) ? memberIdMap.get(memberId) : null;
                    if (member != null) {
                        member.setCaptain(officer);
                    }
                }
            }
        }

        if (!args.contains(ARG_NO_VARIANTS)) {
            Map<Integer, ModuleDef> moduleIdMap = new HashMap<>(10);
            index = 0;
            while (true) {
                index = data.indexOf("{V|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 3;
                }

                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                int memberId = Integer.parseInt(data.substring(index, endIndex));
                FleetMemberAPI member = (memberIdMap != null) ? memberIdMap.get(memberId) : null;
                if (member == null) {
                    continue;
                }

                ShipVariantAPI variant = member.getVariant().clone();
                variant.setSource(VariantSource.REFIT);
                stripWeapons(variant);
                stripFighters(variant);
                variant.setNumFluxCapacitors(0);
                variant.setNumFluxVents(0);
                variant.clearHullMods();

                boolean hasWeapons;
                index = endIndex + 1;
                endIndex = Math.min(data.indexOf(':', index), data.indexOf('|', index));
                if (endIndex == -1) {
                    hasWeapons = false;
                    endIndex = data.indexOf('|', index);
                } else {
                    hasWeapons = (data.indexOf(':', index) == endIndex);
                }
                if (endIndex == -1) {
                    return false;
                }
                if (hasWeapons) {
                    while (true) {
                        String slotId = data.substring(index, endIndex);

                        boolean hasNext;
                        index = endIndex + 1;
                        endIndex = Math.min(data.indexOf('\\', index), data.indexOf('|', index));
                        if (endIndex == -1) {
                            hasNext = false;
                            endIndex = data.indexOf('|', index);
                        } else {
                            hasNext = (data.indexOf('\\', index) == endIndex);
                        }
                        if (endIndex == -1) {
                            return false;
                        }

                        String weaponId = data.substring(index, endIndex);
                        WeaponSpecAPI spec;
                        try {
                            spec = Global.getSettings().getWeaponSpec(weaponId);
                        } catch (Exception e) {
                            spec = null;
                        }
                        if (spec != null) {
                            WeaponSlotAPI slot;
                            try {
                                slot = variant.getSlot(slotId);
                            } catch (Exception e) {
                                slot = null;
                            }
                            if (slot != null) {
                                boolean compatible = true;
                                boolean allowSizeSmaller = true;
                                switch (slot.getWeaponType()) {
                                    case BALLISTIC:
                                        compatible &= (spec.getType() == WeaponType.BALLISTIC);
                                        allowSizeSmaller = true;
                                        break;
                                    case MISSILE:
                                        compatible &= (spec.getType() == WeaponType.MISSILE);
                                        allowSizeSmaller = true;
                                        break;
                                    case ENERGY:
                                        compatible &= (spec.getType() == WeaponType.ENERGY);
                                        allowSizeSmaller = true;
                                        break;
                                    case UNIVERSAL:
                                        compatible &= (spec.getType() == WeaponType.BALLISTIC ||
                                                       spec.getType() == WeaponType.MISSILE ||
                                                       spec.getType() == WeaponType.ENERGY);
                                        allowSizeSmaller = false;
                                        break;
                                    case HYBRID:
                                        compatible &= (spec.getType() == WeaponType.BALLISTIC ||
                                                       spec.getType() == WeaponType.ENERGY);
                                        allowSizeSmaller = false;
                                        break;
                                    case SYNERGY:
                                        compatible &= (spec.getType() == WeaponType.MISSILE ||
                                                       spec.getType() == WeaponType.ENERGY);
                                        allowSizeSmaller = false;
                                        break;
                                    case COMPOSITE:
                                        compatible &= (spec.getType() == WeaponType.BALLISTIC ||
                                                       spec.getType() == WeaponType.MISSILE);
                                        allowSizeSmaller = false;
                                        break;
                                    default:
                                        break;
                                }
                                switch (slot.getSlotSize()) {
                                    case SMALL:
                                        compatible &= (spec.getSize() == WeaponSize.SMALL);
                                        break;
                                    case MEDIUM:
                                        compatible &= (spec.getSize() == WeaponSize.MEDIUM ||
                                                       (spec.getSize() == WeaponSize.SMALL && allowSizeSmaller));
                                        break;
                                    case LARGE:
                                        compatible &= (spec.getSize() == WeaponSize.LARGE ||
                                                       (spec.getSize() == WeaponSize.MEDIUM && allowSizeSmaller));
                                        break;
                                    default:
                                        break;
                                }
                                if (compatible) {
                                    if (storage.getNumWeapons(weaponId) > 0) {
                                        variant.addWeapon(slotId, weaponId);
                                        int cost = variant.computeOPCost(playerStats);
                                        if (cost > variant.getHullSpec().getOrdnancePoints(playerStats)) {
                                            variant.clearSlot(slotId);
                                        } else {
                                            storage.removeWeapons(weaponId, 1);
                                        }
                                    }
                                }
                            }
                        }

                        if (!hasNext) {
                            break;
                        }
                        index = endIndex + 1;
                        endIndex = data.indexOf(':', index);
                        if (endIndex == -1) {
                            return false;
                        }
                    }
                }

                boolean hasModules;
                index = endIndex + 1;
                endIndex = Math.min(data.indexOf(':', index), data.indexOf('|', index));
                if (endIndex == -1) {
                    hasModules = false;
                    endIndex = data.indexOf('|', index);
                } else {
                    hasModules = (data.indexOf(':', index) == endIndex);
                }
                if (endIndex == -1) {
                    return false;
                }
                if (hasModules) {
                    while (true) {
                        String slotId = data.substring(index, endIndex);

                        boolean hasNext;
                        index = endIndex + 1;
                        endIndex = Math.min(data.indexOf('\\', index), data.indexOf('|', index));
                        if (endIndex == -1) {
                            hasNext = false;
                            endIndex = data.indexOf('|', index);
                        } else {
                            hasNext = (data.indexOf('\\', index) == endIndex);
                        }
                        if (endIndex == -1) {
                            return false;
                        }

                        int moduleId = Integer.parseInt(data.substring(index, endIndex));
                        if (variant.getStationModules().containsKey(slotId)) {
                            moduleIdMap.put(moduleId, new ModuleDef(member, slotId));
                        }

                        if (!hasNext) {
                            break;
                        }
                        index = endIndex + 1;
                        endIndex = data.indexOf(':', index);
                        if (endIndex == -1) {
                            return false;
                        }
                    }
                }

                while (true) {
                    boolean hasNext;
                    index = endIndex + 1;
                    endIndex = Math.min(data.indexOf('\\', index), data.indexOf('|', index));
                    if (endIndex == -1) {
                        hasNext = false;
                        endIndex = data.indexOf('|', index);
                    } else {
                        hasNext = (data.indexOf('\\', index) == endIndex);
                    }
                    if (endIndex == -1) {
                        return false;
                    }
                    String wingId = data.substring(index, endIndex);
                    if (wingId == null) {
                        break;
                    }

                    FighterWingSpecAPI spec;
                    try {
                        spec = Global.getSettings().getFighterWingSpec(wingId);
                    } catch (Exception e) {
                        spec = null;
                    }
                    if (spec != null) {
                        if (storage.getNumFighters(wingId) > 0) {
                            for (int i = 0; i < variant.getHullSpec().getFighterBays(); i++) {
                                if (variant.getWingId(i) == null || variant.getWingId(i).isEmpty()) {
                                    variant.setWingId(i, wingId);
                                    int cost = variant.computeOPCost(playerStats);
                                    if (cost > variant.getHullSpec().getOrdnancePoints(playerStats)) {
                                        variant.setWingId(i, "");
                                    } else {
                                        storage.removeItems(CargoItemType.FIGHTER_CHIP, wingId, 1);
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    if (!hasNext) {
                        break;
                    }
                }

                while (true) {
                    boolean hasNext;
                    index = endIndex + 1;
                    endIndex = Math.min(data.indexOf('\\', index), data.indexOf('|', index));
                    if (endIndex == -1) {
                        hasNext = false;
                        endIndex = data.indexOf('|', index);
                    } else {
                        hasNext = (data.indexOf('\\', index) == endIndex);
                    }
                    if (endIndex == -1) {
                        return false;
                    }
                    String modId = data.substring(index, endIndex);
                    if (modId == null) {
                        break;
                    }

                    int cost = 0;
                    boolean isAllowed;
                    try {
                        ShipVariantAPI clone = variant.clone();
                        clone.addMod(modId);
                        cost = clone.computeOPCost(playerStats);
                        isAllowed = true;
                    } catch (Exception e) {
                        isAllowed = false;
                    }
                    if (isAllowed) {
                        if (cost <= variant.getHullSpec().getOrdnancePoints(playerStats)) {
                            variant.addMod(modId);
                        }
                    }

                    if (!hasNext) {
                        break;
                    }
                }

                while (true) {
                    boolean hasNext;
                    index = endIndex + 1;
                    endIndex = Math.min(data.indexOf('\\', index), data.indexOf('|', index));
                    if (endIndex == -1) {
                        hasNext = false;
                        endIndex = data.indexOf('|', index);
                    } else {
                        hasNext = (data.indexOf('\\', index) == endIndex);
                    }
                    if (endIndex == -1) {
                        return false;
                    }
                    String modId = data.substring(index, endIndex);
                    if (modId == null) {
                        break;
                    }

                    boolean isAllowed;
                    try {
                        ShipVariantAPI clone = variant.clone();
                        clone.addPermaMod(modId);
                        clone.computeOPCost(playerStats);
                        isAllowed = true;
                    } catch (Exception e) {
                        isAllowed = false;
                    }
                    if (isAllowed) {
                        if (!variant.hasHullMod(modId)) {
                            variant.addPermaMod(modId);
                        }
                    }

                    if (!hasNext) {
                        break;
                    }
                }

                boolean hasGroups;
                index = endIndex + 1;
                endIndex = Math.min(data.indexOf(':', index), data.indexOf('|', index));
                if (endIndex == -1) {
                    hasGroups = false;
                    endIndex = data.indexOf('|', index);
                } else {
                    hasGroups = (data.indexOf(':', index) == endIndex);
                }
                if (endIndex == -1) {
                    return false;
                }
                if (hasGroups) {
                    int groupIndex = 0;
                    while (true) {
                        WeaponGroupSpec group = variant.getGroup(groupIndex);
                        groupIndex++;
                        if (group == null) {
                            group = new WeaponGroupSpec();
                            variant.addWeaponGroup(group);
                        }
                        group.setAutofireOnByDefault(data.substring(index, endIndex).contentEquals("1"));

                        index = endIndex + 1;
                        endIndex = data.indexOf(':', index);
                        if (endIndex == -1) {
                            return false;
                        }
                        if (data.substring(index, endIndex).contentEquals("1")) {
                            group.setType(WeaponGroupType.ALTERNATING);
                        } else {
                            group.setType(WeaponGroupType.LINKED);
                        }

                        boolean hasNextGroup;
                        while (true) {
                            boolean hasNextSlot;
                            index = endIndex + 1;
                            int a = data.indexOf(':', index);
                            int b = data.indexOf('\\', index);
                            int c = data.indexOf('|', index);
                            if (c == -1) {
                                return false;
                            }
                            if (a == -1) {
                                hasNextSlot = false;
                                if (b == -1) {
                                    endIndex = c;
                                    hasNextGroup = false;
                                } else {
                                    endIndex = Math.min(b, c);
                                    hasNextGroup = (b == endIndex);
                                }
                            } else {
                                if (b == -1) {
                                    endIndex = Math.min(a, c);
                                    hasNextGroup = false;
                                    hasNextSlot = (a == endIndex);
                                } else {
                                    endIndex = Math.min(a, Math.min(b, c));
                                    hasNextSlot = (a == endIndex);
                                    if (!hasNextSlot) {
                                        hasNextGroup = (b == endIndex);
                                    } else {
                                        hasNextGroup = (b == Math.min(b, c));
                                    }
                                }
                            }
                            String slotId = data.substring(index, endIndex);
                            WeaponSlotAPI slot;
                            try {
                                slot = variant.getSlot(slotId);
                            } catch (Exception e) {
                                slot = null;
                            }
                            if (slot != null && variant.getWeaponId(slotId) != null) {
                                group.addSlot(slotId);
                            }

                            if (!hasNextSlot) {
                                break;
                            }
                        }

                        if (!hasNextGroup) {
                            break;
                        }

                        index = endIndex + 1;
                        endIndex = data.indexOf(':', index);
                        if (endIndex == -1) {
                            return false;
                        }
                    }
                }

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                int remainingOP = variant.getHullSpec().getOrdnancePoints(playerStats) - variant.computeOPCost(
                    playerStats);
                variant.setNumFluxVents(Math.min(Integer.parseInt(data.substring(index, endIndex)), remainingOP));

                index = endIndex + 1;
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                remainingOP = variant.getHullSpec().getOrdnancePoints(playerStats) - variant.computeOPCost(playerStats);
                variant.setNumFluxCapacitors(Math.min(Integer.parseInt(data.substring(index, endIndex)), remainingOP));

                index = endIndex + 1;
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                variant.setVariantDisplayName(data.substring(index, endIndex));

                member.setVariant(variant, false, true);
            }

            index = 0;
            while (true) {
                index = data.indexOf("{MV|", index);
                if (index < 0) {
                    break;
                } else {
                    index += 4;
                }

                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }

                int moduleId = Integer.parseInt(data.substring(index, endIndex));
                ModuleDef module = moduleIdMap.get(moduleId);

                if (module == null) {
                    continue;
                }

                ShipVariantAPI variant = module.member.getModuleVariant(module.slotId);
                if (variant == null) {
                    continue;
                }

                variant = variant.clone();
                variant.setSource(VariantSource.REFIT);
                stripWeapons(variant);
                stripFighters(variant);
                variant.setNumFluxCapacitors(0);
                variant.setNumFluxVents(0);
                variant.clearHullMods();

                boolean hasWeapons;
                index = endIndex + 1;
                endIndex = Math.min(data.indexOf(':', index), data.indexOf('|', index));
                if (endIndex == -1) {
                    hasWeapons = false;
                    endIndex = data.indexOf('|', index);
                } else {
                    hasWeapons = (data.indexOf(':', index) == endIndex);
                }
                if (endIndex == -1) {
                    return false;
                }
                if (hasWeapons) {
                    while (true) {
                        String slotId = data.substring(index, endIndex);

                        boolean hasNext;
                        index = endIndex + 1;
                        endIndex = Math.min(data.indexOf('\\', index), data.indexOf('|', index));
                        if (endIndex == -1) {
                            hasNext = false;
                            endIndex = data.indexOf('|', index);
                        } else {
                            hasNext = (data.indexOf('\\', index) == endIndex);
                        }
                        if (endIndex == -1) {
                            return false;
                        }

                        String weaponId = data.substring(index, endIndex);
                        WeaponSpecAPI spec;
                        try {
                            spec = Global.getSettings().getWeaponSpec(weaponId);
                        } catch (Exception e) {
                            spec = null;
                        }
                        if (spec != null) {
                            WeaponSlotAPI slot;
                            try {
                                slot = variant.getSlot(slotId);
                            } catch (Exception e) {
                                slot = null;
                            }
                            if (slot != null) {
                                boolean compatible = true;
                                boolean allowSizeSmaller = true;
                                switch (slot.getWeaponType()) {
                                    case BALLISTIC:
                                        compatible &= (spec.getType() == WeaponType.BALLISTIC);
                                        allowSizeSmaller = true;
                                        break;
                                    case MISSILE:
                                        compatible &= (spec.getType() == WeaponType.MISSILE);
                                        allowSizeSmaller = true;
                                        break;
                                    case ENERGY:
                                        compatible &= (spec.getType() == WeaponType.ENERGY);
                                        allowSizeSmaller = true;
                                        break;
                                    case UNIVERSAL:
                                        compatible &= (spec.getType() == WeaponType.BALLISTIC ||
                                                       spec.getType() == WeaponType.MISSILE ||
                                                       spec.getType() == WeaponType.ENERGY);
                                        allowSizeSmaller = false;
                                        break;
                                    case HYBRID:
                                        compatible &= (spec.getType() == WeaponType.BALLISTIC ||
                                                       spec.getType() == WeaponType.ENERGY);
                                        allowSizeSmaller = false;
                                        break;
                                    case SYNERGY:
                                        compatible &= (spec.getType() == WeaponType.MISSILE ||
                                                       spec.getType() == WeaponType.ENERGY);
                                        allowSizeSmaller = false;
                                        break;
                                    case COMPOSITE:
                                        compatible &= (spec.getType() == WeaponType.BALLISTIC ||
                                                       spec.getType() == WeaponType.MISSILE);
                                        allowSizeSmaller = false;
                                        break;
                                    default:
                                        break;
                                }
                                switch (slot.getSlotSize()) {
                                    case SMALL:
                                        compatible &= (spec.getSize() == WeaponSize.SMALL);
                                        break;
                                    case MEDIUM:
                                        compatible &= (spec.getSize() == WeaponSize.MEDIUM ||
                                                       (spec.getSize() == WeaponSize.SMALL && allowSizeSmaller));
                                        break;
                                    case LARGE:
                                        compatible &= (spec.getSize() == WeaponSize.LARGE ||
                                                       (spec.getSize() == WeaponSize.MEDIUM && allowSizeSmaller));
                                        break;
                                    default:
                                        break;
                                }
                                if (compatible) {
                                    if (storage.getNumWeapons(weaponId) > 0) {
                                        variant.addWeapon(slotId, weaponId);
                                        int cost = variant.computeOPCost(playerStats);
                                        if (cost > variant.getHullSpec().getOrdnancePoints(playerStats)) {
                                            variant.clearSlot(slotId);
                                        } else {
                                            storage.removeWeapons(weaponId, 1);
                                        }
                                    }
                                }
                            }
                        }

                        if (!hasNext) {
                            break;
                        }
                        index = endIndex + 1;
                        endIndex = data.indexOf(':', index);
                        if (endIndex == -1) {
                            return false;
                        }
                    }
                }

                while (true) {
                    boolean hasNext;
                    index = endIndex + 1;
                    endIndex = Math.min(data.indexOf('\\', index), data.indexOf('|', index));
                    if (endIndex == -1) {
                        hasNext = false;
                        endIndex = data.indexOf('|', index);
                    } else {
                        hasNext = (data.indexOf('\\', index) == endIndex);
                    }
                    if (endIndex == -1) {
                        return false;
                    }
                    String wingId = data.substring(index, endIndex);
                    if (wingId == null) {
                        break;
                    }

                    FighterWingSpecAPI spec;
                    try {
                        spec = Global.getSettings().getFighterWingSpec(wingId);
                    } catch (Exception e) {
                        spec = null;
                    }
                    if (spec != null) {
                        if (storage.getNumFighters(wingId) > 0) {
                            for (int i = 0; i < variant.getHullSpec().getFighterBays(); i++) {
                                if (variant.getWingId(i) == null || variant.getWingId(i).isEmpty()) {
                                    variant.setWingId(i, wingId);
                                    int cost = variant.computeOPCost(playerStats);
                                    if (cost > variant.getHullSpec().getOrdnancePoints(playerStats)) {
                                        variant.setWingId(i, "");
                                    } else {
                                        storage.addFighters(wingId, -1);
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    if (!hasNext) {
                        break;
                    }
                }

                while (true) {
                    boolean hasNext;
                    index = endIndex + 1;
                    endIndex = Math.min(data.indexOf('\\', index), data.indexOf('|', index));
                    if (endIndex == -1) {
                        hasNext = false;
                        endIndex = data.indexOf('|', index);
                    } else {
                        hasNext = (data.indexOf('\\', index) == endIndex);
                    }
                    if (endIndex == -1) {
                        return false;
                    }
                    String modId = data.substring(index, endIndex);
                    if (modId == null) {
                        break;
                    }

                    int cost = 0;
                    boolean isAllowed;
                    try {
                        ShipVariantAPI clone = variant.clone();
                        clone.addMod(modId);
                        cost = clone.computeOPCost(playerStats);
                        isAllowed = true;
                    } catch (Exception e) {
                        isAllowed = false;
                    }
                    if (isAllowed) {
                        if (cost <= variant.getHullSpec().getOrdnancePoints(playerStats)) {
                            variant.addMod(modId);
                        }
                    }

                    if (!hasNext) {
                        break;
                    }
                }

                while (true) {
                    boolean hasNext;
                    index = endIndex + 1;
                    endIndex = Math.min(data.indexOf('\\', index), data.indexOf('|', index));
                    if (endIndex == -1) {
                        hasNext = false;
                        endIndex = data.indexOf('|', index);
                    } else {
                        hasNext = (data.indexOf('\\', index) == endIndex);
                    }
                    if (endIndex == -1) {
                        return false;
                    }
                    String modId = data.substring(index, endIndex);
                    if (modId == null) {
                        break;
                    }

                    boolean isAllowed;
                    try {
                        ShipVariantAPI clone = variant.clone();
                        clone.addPermaMod(modId);
                        clone.computeOPCost(playerStats);
                        isAllowed = true;
                    } catch (Exception e) {
                        isAllowed = false;
                    }
                    if (isAllowed) {
                        if (!variant.hasHullMod(modId)) {
                            variant.addPermaMod(modId);
                        }
                    }

                    if (!hasNext) {
                        break;
                    }
                }

                boolean hasGroups;
                index = endIndex + 1;
                endIndex = Math.min(data.indexOf(':', index), data.indexOf('|', index));
                if (endIndex == -1) {
                    hasGroups = false;
                    endIndex = data.indexOf('|', index);
                } else {
                    hasGroups = (data.indexOf(':', index) == endIndex);
                }
                if (endIndex == -1) {
                    return false;
                }
                if (hasGroups) {
                    int groupIndex = 0;
                    while (true) {
                        WeaponGroupSpec group = variant.getGroup(groupIndex);
                        groupIndex++;
                        if (group == null) {
                            group = new WeaponGroupSpec();
                            variant.addWeaponGroup(group);
                        }
                        group.setAutofireOnByDefault(data.substring(index, endIndex).contentEquals("1"));

                        index = endIndex + 1;
                        endIndex = data.indexOf(':', index);
                        if (endIndex == -1) {
                            return false;
                        }
                        if (data.substring(index, endIndex).contentEquals("1")) {
                            group.setType(WeaponGroupType.ALTERNATING);
                        } else {
                            group.setType(WeaponGroupType.LINKED);
                        }

                        boolean hasNextGroup;
                        while (true) {
                            boolean hasNextSlot;
                            index = endIndex + 1;
                            int a = data.indexOf(':', index);
                            int b = data.indexOf('\\', index);
                            int c = data.indexOf('|', index);
                            if (c == -1) {
                                return false;
                            }
                            if (a == -1) {
                                hasNextSlot = false;
                                if (b == -1) {
                                    endIndex = c;
                                    hasNextGroup = false;
                                } else {
                                    endIndex = Math.min(b, c);
                                    hasNextGroup = (b == endIndex);
                                }
                            } else {
                                if (b == -1) {
                                    endIndex = Math.min(a, c);
                                    hasNextGroup = false;
                                    hasNextSlot = (a == endIndex);
                                } else {
                                    endIndex = Math.min(a, Math.min(b, c));
                                    hasNextSlot = (a == endIndex);
                                    if (!hasNextSlot) {
                                        hasNextGroup = (b == endIndex);
                                    } else {
                                        hasNextGroup = (b == Math.min(b, c));
                                    }
                                }
                            }
                            String slotId = data.substring(index, endIndex);
                            WeaponSlotAPI slot;
                            try {
                                slot = variant.getSlot(slotId);
                            } catch (Exception e) {
                                slot = null;
                            }
                            if (slot != null && variant.getWeaponId(slotId) != null) {
                                group.addSlot(slotId);
                            }

                            if (!hasNextSlot) {
                                break;
                            }
                        }

                        if (!hasNextGroup) {
                            break;
                        }

                        index = endIndex + 1;
                        endIndex = data.indexOf(':', index);
                        if (endIndex == -1) {
                            return false;
                        }
                    }
                }

                index = endIndex + 1;
                endIndex = data.indexOf('|', index);
                if (endIndex == -1) {
                    return false;
                }
                int remainingOP = variant.getHullSpec().getOrdnancePoints(playerStats) - variant.computeOPCost(
                    playerStats);
                variant.setNumFluxVents(Math.min(Integer.parseInt(data.substring(index, endIndex)), remainingOP));

                index = endIndex + 1;
                endIndex = data.indexOf('}', index);
                if (endIndex == -1) {
                    return false;
                }
                remainingOP = variant.getHullSpec().getOrdnancePoints(playerStats) - variant.computeOPCost(playerStats);
                variant.setNumFluxCapacitors(Math.min(Integer.parseInt(data.substring(index, endIndex)), remainingOP));

                index = endIndex + 1;
                endIndex = data.indexOf('{', index);
                if (endIndex == -1) {
                    endIndex = data.length();
                }
                variant.setVariantDisplayName(data.substring(index, endIndex));

                try {
                    module.member.setModuleVariant(module.slotId, variant);
                } catch (Exception e) {
                }
            }
        }

        storage.sort();
        if (!args.contains(ARG_NO_CARGO)) {
            if (omnifactory != null) {
                index = 0;
                while (true) {
                    index = data.indexOf("{OF|W|", index);
                    if (index < 0) {
                        break;
                    } else {
                        index += 6;
                    }
                    endIndex = data.indexOf('}', index);
                    if (endIndex == -1) {
                        return false;
                    }
                    String weaponId = data.substring(index, endIndex);
                    try {
                        Global.getSettings().getWeaponSpec(weaponId);
                    } catch (RuntimeException ex) {
                        continue;
                    }
                    index = endIndex + 1;
                    endIndex = data.indexOf('{', index);
                    if (endIndex == -1) {
                        endIndex = data.length();
                    }
                    int size = Integer.parseInt(data.substring(index, endIndex));
                    omnifactory.addWeapons(weaponId, size);
                }

                index = 0;
                while (true) {
                    index = data.indexOf("{OF|S|", index);
                    if (index < 0) {
                        break;
                    } else {
                        index += 6;
                    }
                    endIndex = data.indexOf('}', index);
                    if (endIndex == -1) {
                        return false;
                    }
                    String hullId = data.substring(index, endIndex);
                    index = endIndex + 1;
                    endIndex = data.indexOf('{', index);
                    if (endIndex == -1) {
                        endIndex = data.length();
                    }
                    String shipName = data.substring(index, endIndex);
                    FleetMemberAPI member;
                    try {
                        member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, hullId + "_Hull");
                    } catch (RuntimeException ex) {
                        continue;
                    }
                    member.setShipName(shipName);
                    omnifactory.getMothballedShips().addFleetMember(member);
                }

                index = 0;
                while (true) {
                    index = data.indexOf("{OF|F|", index);
                    if (index < 0) {
                        break;
                    } else {
                        index += 6;
                    }
                    endIndex = data.indexOf('}', index);
                    if (endIndex == -1) {
                        return false;
                    }
                    String wingId = data.substring(index, endIndex);
                    FleetMemberAPI member;
                    try {
                        member = Global.getFactory().createFleetMember(FleetMemberType.FIGHTER_WING, wingId);
                    } catch (RuntimeException ex) {
                        continue;
                    }
                    omnifactory.getMothballedShips().addFleetMember(member);
                }
            }
        }

        if (!args.contains(ARG_NO_EXERELIN_MARKETS)) {
            if (nexerelinExists) {
                String playerAlignedFactionId = PlayerFactionStore.getPlayerFactionId();

                index = 0;
                while (true) {
                    index = data.indexOf("{NPM}", index);
                    if (index < 0) {
                        break;
                    } else {
                        index += 5;
                    }
                    endIndex = data.indexOf('{', index);
                    if (endIndex == -1) {
                        endIndex = data.length();
                    }
                    String marketId = data.substring(index, endIndex);
                    MarketAPI thisMarket = Global.getSector().getEconomy().getMarket(marketId);
                    if (thisMarket != null && !thisMarket.getFactionId().contentEquals(playerAlignedFactionId)) {
                        SectorManager.captureMarket(thisMarket, Global.getSector().getFaction(playerAlignedFactionId),
                                                    thisMarket.getFaction(), false,
                                                    new ArrayList<String>(0), 0f);
                    }
                }

                index = 0;
                while (true) {
                    index = data.indexOf("{NM|", index);
                    if (index < 0) {
                        break;
                    } else {
                        index += 4;
                    }
                    endIndex = data.indexOf('}', index);
                    if (endIndex == -1) {
                        return false;
                    }
                    String factionId = data.substring(index, endIndex);

                    index = endIndex + 1;
                    endIndex = data.indexOf('{', index);
                    if (endIndex == -1) {
                        endIndex = data.length();
                    }
                    String marketId = data.substring(index, endIndex);
                    MarketAPI thisMarket = Global.getSector().getEconomy().getMarket(marketId);
                    if (thisMarket != null && !thisMarket.getFactionId().contentEquals(factionId)) {
                        SectorManager.captureMarket(thisMarket, Global.getSector().getFaction(factionId),
                                                    thisMarket.getFaction(), false,
                                                    new ArrayList<String>(0), 0f);
                    }
                }
            }
        }

        if (!args.contains(ARG_NO_EXERELIN_REP)) {
            if (nexerelinExists) {
                index = 0;
                while (true) {
                    index = data.indexOf("{NF|", index);
                    if (index < 0) {
                        break;
                    } else {
                        index += 4;
                    }
                    endIndex = data.indexOf('|', index);
                    if (endIndex == -1) {
                        return false;
                    }
                    String factionId = data.substring(index, endIndex);

                    index = endIndex + 1;
                    endIndex = data.indexOf('}', index);
                    if (endIndex == -1) {
                        return false;
                    }
                    String otherFactionId = data.substring(index, endIndex);

                    index = endIndex + 1;
                    endIndex = data.indexOf('{', index);
                    if (endIndex == -1) {
                        endIndex = data.length();
                    }
                    float relationship = Float.parseFloat(data.substring(index, endIndex));

                    FactionAPI faction = Global.getSector().getFaction(factionId);
                    FactionAPI otherFaction = Global.getSector().getFaction(otherFactionId);
                    if (faction != null && otherFaction != null) {
                        faction.setRelationship(otherFactionId, relationship);
                    }
                }
            }
        }

        if (!args.contains(ARG_NO_EXERELIN_ALLIANCES)) {
            if (nexerelinExists) {
                index = 0;
                while (true) {
                    index = data.indexOf("{NA|", index);
                    if (index < 0) {
                        break;
                    } else {
                        index += 4;
                    }
                    endIndex = data.indexOf('|', index);
                    if (endIndex == -1) {
                        return false;
                    }
                    String alignmentName = data.substring(index, endIndex);
                    Alignment alignment;
                    try {
                        alignment = Alignment.valueOf(alignmentName);
                    } catch (IllegalArgumentException ex) {
                        alignment = Alignment.values()[0];
                    }

                    /* The size of this section is guaranteed to be 2+ */
                    List<String> members = new ArrayList<>(10);
                    boolean hasNext = true;
                    while (hasNext) {
                        index = endIndex + 1;
                        endIndex = Math.min(data.indexOf('\\', index), data.indexOf('}', index));
                        if (endIndex == -1) {
                            endIndex = data.indexOf('}', index);
                            if (endIndex == -1) {
                                return false;
                            } else {
                                hasNext = false;
                            }
                        } else {
                            hasNext = (endIndex == data.indexOf('\\', index));
                        }
                        String member = data.substring(index, endIndex);
                        if (Global.getSector().getFaction(member) != null) {
                            members.add(member);
                        }
                    }

                    /* This section is currently unused */
                    index = endIndex + 1;
                    endIndex = data.indexOf('{', index);
                    if (endIndex == -1) {
                        endIndex = data.length();
                    }
                    String allianceName = data.substring(index, endIndex);

                    if (members.size() >= 2) {
                        Alliance alliance = AllianceManager.createAlliance(members.get(0), members.get(1), alignment,
                                                                           allianceName);
                        for (int i = 2; i < members.size(); i++) {
                            // This shouldn't actually create an AllianceManager, just get the current instance
                            AllianceManager.create().joinAlliance(members.get(i), alliance);
                        }
                    }
                }
            }
        }
        return true;
    }

    private static void stripFighters(ShipVariantAPI variant) {
        int numBays = 20; // well above whatever it might actually be
        for (int i = 0; i < numBays; i++) {
            if (variant.getWingId(i) != null) {
                variant.setWingId(i, "");
            }
        }
    }

    private static void stripWeapons(ShipVariantAPI variant) {
        for (String id : variant.getFittedWeaponSlots()) {
            WeaponSlotAPI slot = variant.getSlot(id);
            if (slot.isDecorative() || slot.isBuiltIn() || slot.isHidden() ||
                    slot.isSystemSlot() || slot.isStationModule()) {
                continue;
            }
            variant.clearSlot(id);
        }
    }

    @Override
    public CommandResult runCommand(String args, CommandContext context) {
        boolean nexerelinExists = Global.getSettings().getModManager().isModEnabled("nexerelin");

        if (context != CommandContext.CAMPAIGN_MAP) {
            Console.showMessage(CommonStrings.ERROR_CAMPAIGN_ONLY);
            return CommandResult.WRONG_CONTEXT;
        }

        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable trans = clpbrd.getContents(this);
        String data;
        try {
            data = (String) trans.getTransferData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException | IOException ex) {
            Console.showMessage("Invalid clipboard data!");
            return CommandResult.ERROR;
        }

        String[] argList = args.split("\\s");
        Set<String> filteredArgs = new HashSet<>(argList.length);
        CommandResult ret = null;
        for (String arg : argList) {
            String trim = arg.trim().toLowerCase();
            if (trim.isEmpty()) {
                continue;
            }

            filteredArgs.add(trim);
            switch (trim) {
                case ARG_NO_XP:
                case ARG_NO_SKILLS:
                case ARG_NO_HULLMODS:
                case ARG_NO_REP:
                case ARG_NO_BOUNTIES:
                case ARG_NO_FLEET:
                case ARG_NO_LOCATION:
                case ARG_NO_CREDITS:
                case ARG_NO_CARGO:
                case ARG_NO_OFFICERS:
                case ARG_NO_VARIANTS:
                case ARG_NO_EXERELIN_REP:
                case ARG_NO_EXERELIN_MARKETS:
                case ARG_NO_EXERELIN_ALLIANCES:
                case ARG_UNCOMPRESSED:
                    break;
                case ARG_HELP:
                    ret = CommandResult.SUCCESS;
                    break;
                default:
                    ret = CommandResult.BAD_SYNTAX;
                    break;
            }
            if (ret != null) {
                if (nexerelinExists) {
                    Console.showMessage(
                            "Available flags:\n" +
                            ARG_NO_XP + " : Don't load XP progress\n" +
                            ARG_NO_SKILLS + " : Don't load skill choices\n" +
                            ARG_NO_HULLMODS + " : Don't load known hullmods\n" +
                            ARG_NO_REP + " : Don't load faction reputation\n" +
                            ARG_NO_BOUNTIES + " : Don't load bounty progress\n" +
                            ARG_NO_FLEET + " : Don't load ships/fighters\n" +
                            ARG_NO_LOCATION + " : Don't load player fleet location\n" +
                            ARG_NO_CREDITS + " : Don't load credit count\n" +
                            ARG_NO_CARGO + " : Don't load cargo items\n" +
                            ARG_NO_OFFICERS + " : Don't load officer data\n" +
                            ARG_NO_VARIANTS + " : Don't load ship variants\n" +
                            ARG_NO_EXERELIN_REP + " : Don't load inter-faction reputation (Exerelin)\n" +
                            ARG_NO_EXERELIN_MARKETS + " : Don't load captured markets (Exerelin)\n" +
                            ARG_NO_EXERELIN_ALLIANCES + " : Don't load faction alliances (Exerelin)\n" +
                            ARG_UNCOMPRESSED + " : Load uncompressed data\n" +
                            ARG_HELP + " : Show these options");
                } else {
                    Console.showMessage(
                            "Available flags:\n" +
                            ARG_NO_XP + " : Don't load XP progress\n" +
                            ARG_NO_SKILLS + " : Don't load skill choices\n" +
                            ARG_NO_HULLMODS + " : Don't load known hullmods\n" +
                            ARG_NO_REP + " : Don't load faction reputation\n" +
                            ARG_NO_BOUNTIES + " : Don't load bounty progress\n" +
                            ARG_NO_FLEET + " : Don't load ships/fighters\n" +
                            ARG_NO_LOCATION + " : Don't load player fleet location\n" +
                            ARG_NO_CREDITS + " : Don't load credit count\n" +
                            ARG_NO_CARGO + " : Don't load cargo items\n" +
                            ARG_NO_OFFICERS + " : Don't load officer data\n" +
                            ARG_NO_VARIANTS + " : Don't load ship variants\n" +
                            ARG_UNCOMPRESSED + " : Load uncompressed data\n" +
                            ARG_HELP + " : Show these options");
                }
                return ret;
            }
        }

        try {
            if (!filteredArgs.contains(ARG_UNCOMPRESSED)) {
                data = decompressData(data);
            }
        } catch (Exception ex) {
            Console.showMessage("Could not decompress data!");
            return CommandResult.ERROR;
        }

        if (deserializePlayerData(data, filteredArgs)) {
            CargoAPI storage = Storage.retrieveStorage();
            CargoAPI playerCargo = Global.getSector().getPlayerFleet().getCargo();
            if (storage == playerCargo) {
                Console.showMessage("Save data successfully loaded.");
            } else {
                Console.showMessage(
                        "Save data successfully loaded. Use the \"storage\" command to access weapons and cargo.");
            }
            return CommandResult.SUCCESS;
        } else {
            Console.showMessage("Invalid data!");
            return CommandResult.ERROR;
        }
    }

    private static class ModuleDef {

        FleetMemberAPI member;
        String slotId;

        ModuleDef(FleetMemberAPI member, String slotId) {
            this.member = member;
            this.slotId = slotId;
        }
    }

    private static class OfficerSkillPick {

        int level;
        final String skillId;

        OfficerSkillPick(String skillId, int level) {
            this.skillId = skillId;
            this.level = level;
        }
    }
}
